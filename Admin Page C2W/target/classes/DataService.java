package com.adminpanel.firebaseConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

public class DataService {
    private static Firestore db;

    static {
        try {
            initialFirebase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void initialFirebase() throws IOException {
        FileInputStream serviceAccount = new FileInputStream("src\\main\\resources\\scene-craft-firebase-adminsdk.json");

        @SuppressWarnings("deprecation")
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://scene-craft-default-rtdb.asia-southeast1.firebasedatabase.app")
                .build();

        FirebaseApp.initializeApp(options);

        db = FirestoreClient.getFirestore();
    }

    public void addData(String collection, String document, Map<String, Object> data) throws ExecutionException, InterruptedException{
        DocumentReference docref = db.collection(collection).document(document);
        ApiFuture<WriteResult> result = docref.set(data);

        result.get();
    }

    public DocumentSnapshot getData(String collection, String document) throws ExecutionException, InterruptedException {
        try {
            DocumentReference docref = db.collection(collection).document(document);
        ApiFuture<DocumentSnapshot> future = docref.get();
        return future.get();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public boolean authenticateUser(String username, String password) throws  ExecutionException, InterruptedException {
        DocumentSnapshot document = db.collection("trial").document(username).get().get();
        if (document.exists()) {
            String storedPassword = document.getString("password");
            return password.equals(storedPassword);
        }
        return false;
    }

    public boolean isAdmin(String username) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'isAdmin'");
    }
}
