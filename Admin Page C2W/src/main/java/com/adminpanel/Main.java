package com.adminpanel;

import com.adminpanel.initialize.InitializeApp;

import javafx.application.Application;

public class Main {
    public static void main(String[] args) {
        Application.launch(InitializeApp.class, args);
    }
}